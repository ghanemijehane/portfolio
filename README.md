**Présentation**
<br>
J'ai réalisé mon portfolio avec HTML, CSS et Bootstrap afin de pouvoir mettre en ligne les futurs projets élaborés durant la formation.
<br>
Le site devait être responsive et validé par W3C.

<hr>

**Maquette**
<br>
![wireframe portfolio](images/maquette.png)

[Lien vers la maquette](https://whimsical.com/portfolio-8vdhPyuXfwUoK9HTcTc9CE
)

<hr>

**Site web portfolio**

[Lien vers mon portfolio](https://ghanemijehane.gitlab.io/portfolio/)
